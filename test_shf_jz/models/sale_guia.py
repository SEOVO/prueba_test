from odoo import api, fields, models, SUPERUSER_ID, _

class SaleGuia(models.Model):
    _name = 'salex.guia'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'date_order desc, id desc'
    name = fields.Char(string='Order Reference', required=True, copy=False, readonly=True,
                       states={'draft': [('readonly', False)]}, index=True, default=lambda self: _('New'))

    date_order = fields.Datetime(string='Order Date', required=True, readonly=True, index=True,
                                 copy=False,
                                 default=fields.Datetime.now,
                                 help="Creation date of draft/sent orders,\nConfirmation date of confirmed orders.")
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('comfirm', 'Aprobada'),
        ('done', 'Completada'),
        ('cancel', 'Cancelada'),
        ('ended', 'Finalizada'),

    ], string='Status', readonly=True, copy=False, index=True, tracking=3, default='draft')

    user_id = fields.Many2one(
        'res.users', string='Salesperson', index=True, tracking=2, default=lambda self: self.env.user,
       )
    partner_id = fields.Many2one(
        'res.partner', string='Cliente', readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        required=True, change_default=True, index=True, tracking=1,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]", )

    note = fields.Text('Observaciones')
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True,
                                 default=lambda self: self.env.company)
    order_line = fields.One2many('salex.guia.line', 'order_id', string='Order Lines',
                                 states={'cancel': [('readonly', True)], 'done': [('readonly', True)]}, copy=True,
                                 )
    amount_total = fields.Float(string='Total', store=True, readonly=True, compute='_amount_all', tracking=4)
    categ_id = fields.Many2one('categoria.guia',string="Categoria")
    @api.depends('order_line')
    def _amount_all(self):
        for record in self:
            amount_total = 0
            for l in record.order_line:
                amount_total += l.price_total
            record.amount_total = amount_total

    my_url = fields.Char(compute="get_my_url")
    def get_my_url(self):
        for record in self:
            record.my_url = f'''/my/guias/{str(record.id)}'''


    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq_date = None
            if 'date_order' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['date_order']))
            if 'company_id' in vals:
                vals['name'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code(
                    'salex.guia', sequence_date=seq_date) or _('New')
            else:
                vals['name'] = self.env['ir.sequence'].next_by_code('salex.guia', sequence_date=seq_date) or _('New')


        result = super(SaleGuia, self).create(vals)
        return result


    def print_cotizacion(self):
        return self.env.ref('test_shf_jz.action_report_saleguia').report_action(self)

    def fun_aprobar(self):
        self.state = 'comfirm'

    def fun_completar(self):
        self.state = 'done'

    def fun_end(self):
        self.state =  'ended'

    def fun_cancelar(self):
        self.state = 'cancel'


class Categoria(models.Model):
    _name = 'categoria.guia'
    name = fields.Char()

class SalexGuiaLine(models.Model):
    _name = 'salex.guia.line'
    _order = "sequence"
    order_id = fields.Many2one('salex.guia', string='Order Reference', required=True, ondelete='cascade', index=True,
                               copy=False)
    name = fields.Char(string="Descripcion")
    sequence = fields.Integer('sequence', help="Sequence for the handle.", default=0)
    display_type = fields.Selection([
        ('line_section', "Section"),
        ('line_note', "Note")], default=False, help="Technical field for UX purpose.")

    product_qty = fields.Float(string="Cantidad")
    price_unit = fields.Float(string="Precio Unitario")
    desc = fields.Float(string="Descuento %")
    price_total = fields.Float(string="Precio Total",compute="get_price_total")
    @api.depends('desc','product_qty','price_unit')
    def get_price_total(self):
        for record in self:
            record.price_total = record.product_qty * record.price_unit * (1- (record.desc/100))




