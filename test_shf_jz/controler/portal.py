# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import binascii
from datetime import date

from odoo import fields, http, _
from odoo.exceptions import AccessError, MissingError
from odoo.http import request
from odoo.addons.payment.controllers.portal import PaymentProcessing
from odoo.addons.portal.controllers.mail import _message_post_helper
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager, get_records_pager
from odoo.osv import expression


class CustomerPortal(CustomerPortal):

    def _prepare_home_portal_values(self):
        values = super(CustomerPortal, self)._prepare_home_portal_values()
        partner = request.env.user.partner_id

        SaleGuia = request.env['salex.guia']
        quotation_count = SaleGuia.search_count([
            ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
            ('state', 'not in', ['draft', 'cancel'])
        ]) if SaleGuia.check_access_rights('read', raise_exception=False) else 0

        values.update({
            'guias_count': quotation_count,
        })
        return values

    @http.route(['/my/guias', '/my/guias/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_orders(self, page=1, date_begin=None, date_end=None, sortby=None, **kw):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        SaleGuia = request.env['salex.guia']

        domain = [

            ('state', 'not in', ['draft', 'cancel'])
        ]

        # ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),

        searchbar_sortings = {
            'date': {'label': _('Order Date'), 'order': 'date_order desc'},
            'name': {'label': _('Reference'), 'order': 'name'},
            'stage': {'label': _('Stage'), 'order': 'state'},
        }
        # default sortby order
        if not sortby:
            sortby = 'date'
        sort_order = searchbar_sortings[sortby]['order']



        # count for pager
        order_count = SaleGuia.search_count(domain)
        # pager
        pager = portal_pager(
            url="/my/guias",
            url_args={'date_begin': date_begin, 'date_end': date_end, 'sortby': sortby},
            total=order_count,
            page=page,
            step=self._items_per_page
        )
        # content according to pager and archive selected
        orders = SaleGuia.search(domain, order=sort_order, limit=self._items_per_page, offset=pager['offset'])
        request.session['my_guias_history'] = orders.ids[:100]

        archive_groups = self._get_archive_groups('salex.guia', domain) if values.get('my_details') else []
        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]

        values.update({
            'date': date_begin,
            'orders': orders.sudo(),
            'page_name': 'order',
            'pager': pager,
            'archive_groups': archive_groups,
            'default_url': '/my/guias',
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby,
        })
        return request.render("test_shf_jz.portal_my_orders", values)

    @http.route(['/my/guias/<int:order_id>'], type='http', auth="public", website=True)
    def portal_order_page(self, order_id, report_type=None, access_token=None, message=False, download=False, **kw):
        try:
            order_sudo = self._document_check_access('salex.guia', order_id, access_token=access_token)
        except (AccessError, MissingError):
            return request.redirect('/my')

        if report_type in ('html', 'pdf', 'text'):
            return self._show_report(model=order_sudo, report_type=report_type,
                                     report_ref='test_shf_jz.action_report_saleguia', download=download)

        # use sudo to allow accessing/viewing orders for public user
        # only if he knows the private token
        # Log only once a day
        if order_sudo:
            now = fields.Date.today().isoformat()
            session_obj_date = request.session.get('view_quote_%s' % order_sudo.id)
            if isinstance(session_obj_date, date):
                session_obj_date = session_obj_date.isoformat()
            if session_obj_date != now and request.env.user.share and access_token:
                request.session['view_quote_%s' % order_sudo.id] = now
                body = _('Quotation viewed by customer %s') % order_sudo.partner_id.name
                _message_post_helper(
                    "sale.order",
                    order_sudo.id,
                    body,
                    token=order_sudo.access_token,
                    message_type="notification",
                    subtype="mail.mt_note",
                    partner_ids=order_sudo.user_id.sudo().partner_id.ids,
                )

        values = {
            'sale_order': order_sudo,
            'message': message,
            'token': access_token,
            'return_url': '/shop/payment/validate',
            'bootstrap_formatting': True,
            'partner_id': order_sudo.partner_id.id,
            'report_type': 'html',


        }
        if order_sudo.company_id:
            values['res_company'] = order_sudo.company_id



        history = request.session.get('my_guias_history', [])
        values.update(get_records_pager(history, order_sudo))

        return request.render('test_shf_jz.sale_order_portal_template', values)

    @http.route(['/my/guias_agrupado', '/my/guias_agrupado/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_ordersx(self, page=1, date_begin=None, date_end=None, sortby=None, **kw):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        SaleGuia = request.env['salex.guia']




        clientes = request.env['res.partner'].search([]).sudo()

        orders = []

        for c in clientes:
            domain = [
                ('partner_id','=',c.id),
                ('state', 'not in', ['draft', 'cancel'])
            ]

            guias = SaleGuia.search(domain).sudo()
            total_monto = 0
            for g in guias:
                total_monto += g.amount_total
            if len(guias) > 0:
                orders.append((c, guias,total_monto))

            #order_count = SaleGuia.search_count(domain)





        values.update({
            'date': date_begin,
            'orders': orders,
            'page_name': 'order',
            'default_url': '/my/guias_agrupado',
            'sortby': sortby,
        })
        return request.render("test_shf_jz.portal_my_orders_agrupado", values)